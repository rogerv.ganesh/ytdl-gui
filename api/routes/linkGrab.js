var express = require('express');
var router = express.Router();
var shell = require('shelljs');

router.get('/', function(req, res, next) {
    res.send('API is working properly');
});

router.post('/', function(req, res, next) {
  var child = shell.exec('youtube-dl -s ' + req.body.url, {async:true, silent:true});
  child.stdout.on('data', function(data){
    req.app.io.to(req.body.id).emit("test", data);
  });
});

module.exports = router;