var express = require('express');
var router = express.Router();
var shell = require('shelljs');

router.get('/', function(req, res, next) {
    res.send('API is working properly');
});

router.post('/', function(req, res, next) {
  shell.exec('../bin/youtube-dl -e --get-thumbnail --get-format ' + req.body.url, 
    function(code, stdout, stderr) {
	    if (!stderr) {
        res.json({
          link_data: stdout.split('\n')
        })
	    }
  });
});

module.exports = router;
