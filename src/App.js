import React, {Component} from 'react';
import socketIOClient from "socket.io-client";
import axios from 'axios';
import URL from './components/URL'
import OptionTabs from './components/OptionTabs'
import Loader from './components/Loader'


import AppBar from '@material-ui/core/AppBar'
import Toolbar from '@material-ui/core/Toolbar'
import IconButton from '@material-ui/core/IconButton'
import MenuIcon from '@material-ui/icons/Menu'
import Typography from '@material-ui/core/Typography'
import { createMuiTheme } from '@material-ui/core/styles';
import { ThemeProvider } from '@material-ui/styles';
import Button from '@material-ui/core/Button';
import { makeStyles } from '@material-ui/core/styles';

import validURL from 'valid-url';
import './css/App.css';
import 'typeface-montserrat/index.css'
import 'typeface-titillium-web/index.css'

class App extends Component {

  theme = createMuiTheme({
    palette: {
      primary: {
        light: '#93f3ff',
        main: '#5BC0EB',
        dark: '#0c90b9',
        contrastText: '#fff',
      },
      secondary: {
        light: '#fa7921',
        main: '#ffaa52',
        dark: '#c04a00',
        contrastText: '#fff',
      },
    },
    typography: {
      fontFamily: [
        '"Titillium Web"',
        'Montserrat',
        'Nunito',
        'Roboto',
        'sans-serif'
      ].join(','),
    },
    body1: {
      fontFamily: [
        'Montserrat',
        '"Titillium Web"',
        'Nunito',
        'Roboto',
        'sans-serif'
      ].join(','),
    },
    body2: {
      fontFamily: [
        'Montserrat',
        '"Titillium Web"',
        'Nunito',
        'Roboto',
        'sans-serif'
      ].join(','),
    }
  });

  state = {
    types: [
      {
        id: 1, 
        title: "Video",
        selected: true
      },
      {
        id: 2,
        title: "Music",
        selected: false
      }
    ],
    url: {
      text: ''
    },
    options: [
      {
        id: 0,
        title: "Summary",
        description: "Summary"
      }, {
        id: 1,
        title: "Video",
        description: "Video Options"
      }, {
        id: 2,
        title: "Audio",
        description: "Audio Options"
      }
    ],
    linkInfo: {
      title: '',
      format: '',
      thumbnail: ''
    },
    serverEndpoint: {
      url:'http://localhost:9000', 
      id: ''
    },
    showLoader: false
  }

  formStyle = {
    padding: '1rem',
    display: 'flex',
    flexDirection: 'row'
  }

  showLoader = () => {
    this.setState({
      showLoader: true
    })
  }

  hideLoader = () => {
    this.setState({
      showLoader: false
    })
  }

  setURL = url => {
    if (validURL.isUri(url)){
      if (url !== this.state.url.text) {
        this.showLoader();
        this.setState({
          url: {
            text: url
          }
        });
        axios({
          method: 'post',
          url: 'http://localhost:9000/linkInfo',
          data: {
            id: this.state.serverEndpoint.id,
            url: url,
          }
        }).then((response) => {
          this.hideLoader();
          this.setState({
            linkInfo: {
              title: response.data.link_data[0],
              format: response.data.link_data[2],
              thumbnail: response.data.link_data[1]
            }
          });
        }, (error) => {
          this.hideLoader();
          console.log(error);
        });
      }
    } else {
      this.hideLoader();
      console.log("not a valid url");
    }
  }

  selectType = id => {
    this.setState({
      types: this.state.types.map(type => {
          type.selected = !type.selected;
        return type;
      })
    });
  }

  updateThumbnail = url => {
    const linkInfo = {...this.state.linkInfo}
    linkInfo.thumbnail = url;
    this.setState({linkInfo})
  }

  componentDidMount() {
    const url = this.state.serverEndpoint.url;
    const socket = socketIOClient(url);
    socket.on("socket_id", data => this.setState({
      serverEndpoint: {
        url: url, 
        id: data
    }}));
    socket.on("test", data => console.log(data));
  }

  useStyles = makeStyles(theme => ({
    backdrop: {
      zIndex: 1000,
      color: '#fff',
    },
  }));

  render () { 
    return (
      <ThemeProvider theme={this.theme}>
        <div className="App">
          <AppBar position="static">
            <Toolbar>
              <IconButton edge="start" color="inherit" aria-label="menu">
                <MenuIcon />
              </IconButton>
              <Typography variant="h6" >
                YTDL-GUI
              </Typography>
            </Toolbar>
          </AppBar>
          <div className="container">
            <div className="row inputOptions">
              {/* <div className="flex-column">
                <Type selectType={this.selectType} types={this.state.types} />
              </div> */}
              <div className="col-11 col-sm-11 col-md-11 col-lg-11 col-xl-11">
                <URL setURL={this.setURL} />
              </div>
              <div className="col-1 col-sm-1 col-md-1 col-lg-1 col-xl-1">
              <Button 
                variant="contained"
                color={(this.state.linkInfo.title ? 'secondary' : 'disabled')}>
                Download
              </Button>
              </div>
            </div>
          </div>
          <OptionTabs options={this.state.options} linkData={this.state.linkInfo} updateThumbnail={this.updateThumbnail}/>
          {/* <Notification message="test" severity="warning"/> */}
        </div>
        <Loader open={this.state.showLoader} />
        {/* <div>
          <Loader ></Loader>
        </div> */}
        {/* <Backdrop style={this.useStyles.backdrop} open={this.state.showLoader}>
          <CircularProgress color="primary" />
        </Backdrop> */}
      </ThemeProvider>
    );
  }
}

export default App;
