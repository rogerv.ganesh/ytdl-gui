import React from 'react'
import { makeStyles } from '@material-ui/core/styles';
import InputLabel from '@material-ui/core/InputLabel';
import MenuItem from '@material-ui/core/MenuItem';
import FormControl from '@material-ui/core/FormControl';
import Select from '@material-ui/core/Select';

const useStyles = makeStyles(theme => ({
  formControl: {
    marginLeft: theme.spacing(2),
    minWidth: 120,
  },
  selectEmpty: {
    marginTop: theme.spacing(2),
  },
}));

export default function OutputFormat(props) {
  const classes = useStyles();
  const [audioFormats, setAudioFormats] = React.useState([
    {
      id: 0,
      title: "source",
      selected: false
    }, {
      id: 1,
      title: "aac",
      selected: false
    }, {
      id: 2,
      title: "flac",
      selected: false
    }, {
      id: 3,
      title: "mp3",
      selected: true
    }, {
      id: 4,
      title: "m4a",
      selected: false
    }, {
      id: 5,
      title: "opus",
      selected: false
    }, {
      id: 6,
      title: "vorbis",
      selected: false
    }, {
      id: 7,
      title: "wav",
      selected: false
    }
  ]);

  const [videoFormats, setVideoFormats] = React.useState([
    {
      id: 0,
      title: "source",
      selected: false
    }, {
      id: 1,
      title: "mp4",
      selected: false
    }, {
      id: 2,
      title: "flv",
      selected: false
    }, {
      id: 3,
      title: "ogg",
      selected: true
    }, {
      id: 4,
      title: "webm",
      selected: false
    }, {
      id: 5,
      title: "mkv",
      selected: false
    }, {
      id: 6,
      title: "flv",
      selected: false
    }
  ]);

  const inputLabel = React.useRef(null);

  const [labelWidth, setLabelWidth] = React.useState(0);

  React.useEffect(() => {
    setLabelWidth(inputLabel.current.offsetWidth);
  }, []);

  const audioItems = audioFormats.map((format) => (
    <MenuItem key={format.id} value={format.title}>
      {format.title}
    </MenuItem>
  ));

  const videoItems = videoFormats.map((format) => (
    <MenuItem key={format.id} value={format.title}>
      {format.title}
    </MenuItem>
  ));

  return (
    <FormControl variant="filled" className={classes.formControl}>
      <InputLabel id="demo-simple-select-outlined-label" ref={inputLabel}>File Format</InputLabel>
      <Select
        labelId="demo-simple-select-outlined-label"
        id="demo-simple-select-filled"
        value={props.format}
        onChange={props.selectFormat}
      >
      {(props.types[0].selected ? videoItems : audioItems)}
      </Select>
    </FormControl>
  )
}
