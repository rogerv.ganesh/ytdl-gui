import React from 'react';
import SwipeableViews from 'react-swipeable-views';
import PropTypes from 'prop-types';
import { makeStyles, useTheme } from '@material-ui/core/styles';
import Tabs from '@material-ui/core/Tabs';
import Tab from '@material-ui/core/Tab';
import Typography from '@material-ui/core/Typography';
import Box from '@material-ui/core/Box';


import SummaryOptions from './SummaryOptions'
import AudioOptions from './AudioOptions'

function TabPanel(props) {
  const { children, value, index, ...other } = props;

  return (
    <Typography
      component="div"
      role="tabpanel"
      hidden={value !== index}
      id={`vertical-tabpanel-${index}`}
      aria-labelledby={`vertical-tab-${index}`}
      {...other}
    >
      {value === index && <Box p={3}>{children}</Box>}
    </Typography>
  );
}

TabPanel.propTypes = {
  children: PropTypes.node,
  index: PropTypes.any.isRequired,
  value: PropTypes.any.isRequired,
};

function a11yProps(index) {
  return {
    id: `vertical-tab-${index}`,
    'aria-controls': `vertical-tabpanel-${index}`,
  };
}

const useStyles = makeStyles(theme => ({
  root: {
    flexGrow: 1,
    backgroundColor: theme.palette.background.paper,
    display: 'flex',
    minHeight: '75vh'
  },
  tabs: {
    borderRight: `1px solid ${theme.palette.divider}`,
    minWidth: '160px',
    maxWidth: '160px'
  },
}));

export default function OptionTabs(props) {
  const classes = useStyles();
  const theme = useTheme();
  const [value, setValue] = React.useState(0);
  const [types, setTypes] = React.useState([
    {
      id: 0, 
      title: "Video",
      selected: true
    },
    {
      id: 1,
      title: "Music",
      selected: false
    }
  ])
  const [audioFormat, setAudioFormat] = React.useState('source');
  const [videoFormat, setVideoFormat] = React.useState('source');

  const handleChange = (event, newValue) => {
    setValue(newValue);
  };

  const handleChangeIndex = (event, newValue) => {
    setValue(newValue);
  };

  const selectType = (id) => {
    const newTypes = [...types];
    newTypes.map(type => {
      if (type.id === id) {
        type.selected = true;
      } else {
        type.selected = false;
      }
    return type;
    })
    setTypes(newTypes);
  }

  const selectFormat = e => {
    if (types[0].selected) {
      setVideoFormat(e.target.value);
    } else {
      setAudioFormat(e.target.value);
    }
  }

  const optionTabs =  props.options.map((option) => (
    <Tab key={option.id} label={option.title} {...a11yProps(option.id)} />
  ));
  const optionPanels =  props.options.map((option) => {

    switch(option.id) {
      case 0:
        return (<TabPanel key={option.id} value={value} index={option.id} dir={theme.direction}>
          <Typography 
            variant="h5">
            {option.description}
          </Typography>
          <SummaryOptions 
            types={types}
            mediaFormat={(types[0].selected ? videoFormat : audioFormat)}
            selectType={selectType}
            selectFormat={selectFormat}
            updateThumbnail={props.updateThumbnail}
            linkData={props.linkData}/>
        </TabPanel>)
        break;
      case 2:
        return (<TabPanel key={option.id} value={value} index={option.id} dir={theme.direction}>
          <Typography variant="h5">
            {option.description}
          </Typography>
          <AudioOptions />
        </TabPanel>)
      default:
        return (<TabPanel key={option.id} value={value} index={option.id} dir={theme.direction}>
          <Typography variant="h5">
            {option.description}
          </Typography>
        </TabPanel>)
    }
  });

  return (
    <div className={classes.root}>
      <Tabs
        orientation="vertical"
        value={value}
        onChange={handleChange}
        aria-label="Options"
        className={classes.tabs}
      >
        {optionTabs}
      </Tabs>
      <SwipeableViews
        axis={theme.direction === 'rtl' ? 'x-reverse' : 'x'}
        index={value}
        onChangeIndex={handleChangeIndex}
      >
        {optionPanels}
      </SwipeableViews>
    </div>
  );
}
