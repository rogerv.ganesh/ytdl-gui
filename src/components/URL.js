import React, { Component } from 'react'
import TextField from '@material-ui/core/TextField';
import ClickAwayListener from '@material-ui/core/ClickAwayListener';

export class URL extends Component {
  urlStyle = {
    width: '100%'
  }

  state = {
    url: ''
  }

  onChange = (e) => {
    e.preventDefault();
    this.setState({ [e.target.name]: e.target.value });
  }

  handleClickAway = () => {
    if (this.state.url) {
      this.props.setURL(this.state.url);
    }
  }

  render() {
    return (
      <ClickAwayListener onClickAway={this.handleClickAway}>
        <form noValidate autoComplete="off">
          <TextField style={this.urlStyle} label="URL" name="url" type='url' value={this.state.url} onChange={this.onChange}/>
        </form>
      </ClickAwayListener>
    )
  }
}

export default URL
