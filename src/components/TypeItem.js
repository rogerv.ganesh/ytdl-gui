import React, { Component } from 'react'

import FormControlLabel from '@material-ui/core/FormControlLabel';
import Checkbox from '@material-ui/core/Checkbox';


export class TypeItem extends Component {
  render() {
    const { selected, title, id } = this.props.type;
    return (
      <FormControlLabel
        control={< Checkbox checked={selected} onClick={this.props.selectType.bind(this, id)}/>}
        label={title}
      />
    )
  }
}

export default TypeItem

