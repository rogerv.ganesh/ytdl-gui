import React from 'react';
import Button from '@material-ui/core/Button';
import TextField from '@material-ui/core/TextField';
import Dialog from '@material-ui/core/Dialog';
import DialogActions from '@material-ui/core/DialogActions';
import DialogContent from '@material-ui/core/DialogContent';
import DialogContentText from '@material-ui/core/DialogContentText';
import DialogTitle from '@material-ui/core/DialogTitle';
import Slide from '@material-ui/core/Slide';

const Transition = React.forwardRef(function Transition(props, ref) {
  return <Slide direction="up" ref={ref} {...props} />;
});

export default function UpdateThumbnail(props) {
  const [open, setOpen] = React.useState(false);
  const [URL, setURL] = React.useState('');

  const handleClickOpen = () => {
    setOpen(true);
  };

  const handleClose = () => {
    setOpen(false);
  };

  const handleUpdate = (e) => {
    e.preventDefault()
    if (URL !== '') {
      props.updateThumbnail(URL);
    }
    setOpen(false);
  }

  return (
    <div>
      <Button 
        variant="contained"
        color='secondary'
        onClick={handleClickOpen}
        fullWidth='true'>
        Change Album Art
      </Button>
      <Dialog 
        open={open}
        TransitionComponent={Transition}
        keepMounted
        onClose={handleClose}
        aria-labelledby="form-dialog-title">
        <DialogTitle id="form-dialog-title">Change Thumbnail</DialogTitle>
        <DialogContent>
          <DialogContentText>
            Enter the URL of the new thumbnail
          </DialogContentText>
          <TextField
            autoFocus
            margin="dense"
            id="thumbnail_url"
            label="URL"
            type="url"
            fullWidth
            onChange={e => setURL(e.target.value)}
          />
        </DialogContent>
        <DialogActions>
          <Button onClick={handleClose} color="secondary">
            Cancel
          </Button>
          <Button onClick={handleUpdate} color="primary">
            Change
          </Button>
        </DialogActions>
      </Dialog>
    </div>
  );
}
