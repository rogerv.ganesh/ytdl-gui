import React, { Component } from 'react';
import TypeItem from './TypeItem'; 


class Type extends Component {

    typeStyle = {
        display: 'flex',
    }

    render() {
        const typeItems =  this.props.types.map((type) => (
            <TypeItem key={type.id} type={type} selectType={this.props.selectType}/>
        ));
        return (
            <div className="container">
                <div className="row">
                    {typeItems}
                </div>
            </div>
        )
    }
}

export default Type;
