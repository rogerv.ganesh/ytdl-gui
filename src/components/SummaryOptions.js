import React from 'react';
import { makeStyles } from '@material-ui/core/styles';
import TextField from '@material-ui/core/TextField';

import Type from './Type'
import Formats from './Formats'
import ThumbnailUpdate from './UpdateThumbnail'
import ThumbnailPreview from './PreviewThumbnail'

const useStyles = makeStyles(theme => ({
  root: {
    display: 'flex',
    flexWrap: 'wrap',
    '& > *': {
      margin: theme.spacing(0),
    },
  }
}));

export default function AudioOptions(props) {
  const classes = useStyles();
  const [showAudioFormats, setShowAudioFormats] = React.useState(false);
  const [thumbnailURL, setThumbnailURL] = React.useState(props.linkData.thumbnail);
  const [artist, setArtist] = React.useState('');
  const [title, setTitle] = React.useState('');

  React.useEffect(() => {
    setArtist(props.linkData.title.split(" - ")[0]);
    setTitle(props.linkData.title.split(" - ")[1]);
    setThumbnailURL(props.linkData.thumbnail);
  }, [props.linkData.title, props.linkData.thumbnail])

  return (
    <div className={classes.root}>
      <div className="container">
        <div className="row">
          <div className="flex-column">
            <Type selectType={props.selectType} types={props.types} />
          </div>
        </div>
        <div className="row">
          <div className="container">
            <div className="row">
              <div className="col-8	col-sm-8 col-md-8 col-lg-8 col-xl-8 pl-0">
                <div className='container'>
                  <div className='row'>
                    <div className="flex-grow-1">              
                      <TextField 
                        id="file-name-field"
                        label="Save As"
                        variant="outlined"
                        fullWidth='true'
                        style={{ 
                          display: (props.linkData.title ? 'block' : 'none')}}
                        value={(props.mediaFormat === "source" ? props.linkData.title : (props.linkData.title + '.' + props.mediaFormat))}/>
                    </div>
                    <div 
                      style={{ display: (props.linkData.title ? 'block' : 'none') }}>
                      <Formats
                        format={props.mediaFormat}
                        types={props.types}
                        selectFormat={props.selectFormat}/>
                    </div>
                  </div>
                  <div className='row'>
                    <div className='col-3	col-sm-3 col-md-3 col-lg-3 col-xl-3 pl-0'>
                      <TextField 
                          id="artist-field"
                          label="Artist"
                          variant="outlined"
                          fullWidth='true'
                          margin='normal'
                          style={{ 
                            display: (props.types[1].selected && props.linkData.title ? 'block' : 'none')}}
                          value={artist}/>
                    </div>
                    <div className='flex-grow-1'>
                      <TextField 
                          id="title-field"
                          label='Title'
                          variant="outlined"
                          fullWidth='true'
                          margin='normal'
                          style={{ 
                            display: (props.types[1].selected && props.linkData.title ? 'block' : 'none')}}
                          value={title}/>
                    </div>
                  </div>
                </div>
              </div>
              <div className="col-4	col-sm-4 col-md-4 col-lg-4 col-xl-4">
                {/* <img src={thumbnailURL} style={thumbnail}></img> */}
                <ThumbnailPreview url={thumbnailURL} />
                <div 
                  style={{ 
                    display: (props.types[1].selected && props.linkData.title ? 'block' : 'none'),
                    margin: '1rem auto 1rem auto'}}>
                  <ThumbnailUpdate updateThumbnail={props.updateThumbnail}/>
                </div>  
              </div>
            </div>
          </div>
        </div>
      </div>
    </div>
  )
}