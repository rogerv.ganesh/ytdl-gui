YTDL-GUI

A webtool that allows users to use youtube-dl from a web browser instead of the 
commandline.

This project is simply a wrapper for youtube-dl ([https://github.com/ytdl-org/youtube-dl](url)) and an excerise in learning React and sharpening my skills as a full-stack developer.